import angular from 'angular';
import 'angular-spinner';
import AppComponent from './app.component.js';
import config from './config/app.config';
import Common from './common/components/common';
import Events from './events/components';
import 'normalize.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import '../less/styles.less';

angular.module('app', [
  config,
  Common,
  Events,
  'angularSpinner'
])
    .component('app', AppComponent);
