import angular from 'angular';
import EventDetails from './event-detail/event-detail';
import EventList from './event-list/event-list';

export default angular.module('app.components', [
  EventDetails,
  EventList
]).name;
