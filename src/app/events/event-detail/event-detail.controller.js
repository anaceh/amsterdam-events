import _ from 'lodash';

class EventDetailController {

    static get $inject() {
      return ['$routeParams', '$sce', 'dataService'];
    }

    get pageSize() {
      return 15;
    }

    constructor($routeParams, $sce, dataService) {
      this.dataService = dataService;
      this.dataIsLoading = true;
      this.$sce = $sce;

      this.dataService.getEventInfo($routeParams.poiId).then(({description, location}) => {
        this.dataIsLoading = false;
        this.processDescription(description);
        this.initMap(location.point[0].Point.posList);
      }).catch(() => {
        this.dataIsLoading = false;
        this.hasError = true;
      });
    }

    initMap(rawLocation) {
      let parsedLocation = rawLocation.split(' ');
      let location = {latitude: parseFloat(parsedLocation[0]), longitude: parseFloat(parsedLocation[1])};
      this.map = {center: _.clone(location), markerLocation: _.clone(location), zoom: 13};
    }

    processDescription(rawDescription) {
      let texts = _.filter(rawDescription, {lang: 'en-GB'});
      let description = _(texts)
            .filter(
                value => value.type !== 'X-citysdk/accessibility-textual'
            )
            .map(item => item.value)
            .join(' ');

      this.description = this.$sce.trustAsHtml(description);
      this.accessibility = _.chain(texts)
            .filter({
              type: 'X-citysdk/accessibility-textual'
            })
            .map(item => item.value)
            .first()
            .value();
    }
}

export default EventDetailController;
