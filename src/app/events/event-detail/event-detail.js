import angular from 'angular';
import ngSanitize from 'angular-sanitize';
import dataService from '../../common/services/data.service';
import eventDetailComponent from './event-detail.component';

let eventDetailModule = angular.module('event-detail', [
  ngSanitize,
  dataService
])
    .component('eventDetail', eventDetailComponent)
    .name;

export default eventDetailModule;
