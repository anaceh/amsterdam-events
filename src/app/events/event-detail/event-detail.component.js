import controller from './event-detail.controller';
import template from './event-detail.tpl.html';

let eventDetailComponent = {
  restrict: 'E',
  scope: {},
  template,
  controller,
  controllerAs: 'vm',
  bindToController: true
};

export default eventDetailComponent;
