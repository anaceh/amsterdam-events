import controller from './event-list.controller';
import template from './event-list.tpl.html';

let eventListComponent = {
  restrict: 'E',
  scope: {},
  template,
  controller,
  controllerAs: 'vm',
  bindToController: true
};

export default eventListComponent;
