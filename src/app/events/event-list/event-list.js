import angular from 'angular';
import infiniteScroll from 'ng-infinite-scroll';
import dataService from '../../common/services/data.service';
import eventListComponent from './event-list.component';
import eventTile from './event-tile/event-tile';

let eventListModule = angular.module('event-list', [
  infiniteScroll,
  dataService,
  eventTile
])
    .component('eventList', eventListComponent)
    .name;

export default eventListModule;
