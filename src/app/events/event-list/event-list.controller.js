class EventListController {

    static get $inject() {
      return ['dataService'];
    }

    get pageSize() {
      return 15;
    }

    constructor(dataService) {
      this.dataService = dataService;
      this.pageNumber = 0;
      this.events = [];
      this.noMoreData = false;
    }

    fetchData() {
      this.dataIsLoading = true;
      this.dataService.getFestivals({
        limit: this.pageSize,
        offset: this.pageNumber
      }).then(events => {
        this.pageNumber++;
        this.events = this.events.concat(events);
        this.noMoreData = events.length !== this.pageSize;
        this.dataIsLoading = false;
      });
    }
}

export default EventListController;
