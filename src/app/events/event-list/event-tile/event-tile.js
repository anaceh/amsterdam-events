import angular from 'angular';
import eventTileComponent from './event-tile.component';

let eventTileModule = angular.module('event-tile', [
])
    .component('eventTile', eventTileComponent)
    .name;

export default eventTileModule;
