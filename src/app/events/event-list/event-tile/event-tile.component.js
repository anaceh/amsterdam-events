import template from './event-tile.tpl.html';

let eventTileComponent = {
  restrict: 'E',
  scope: {},
  bindings: {event: '<'},
  template,
  controllerAs: 'vm',
  bindToController: true
};

export default eventTileComponent;
