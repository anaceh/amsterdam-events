routing.$inject = ['$locationProvider', '$routeProvider'];

export default function routing($locationProvider, $routeProvider) {
  $locationProvider.hashPrefix('!');

  $routeProvider.when('/events', {
    template: '<event-list></event-list>'
  }).when('/events/:poiId', {
    template: '<event-detail></event-detail>'
  }).otherwise('/events');
}
