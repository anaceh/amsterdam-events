import ngRouter from 'angular-route';
import 'angular-google-maps';
import 'angular-simple-logger';
import googleMaps from './app.config.google-maps';
import routing from './app.config.routing';

export default angular.module('app.config', [ngRouter, 'uiGmapgoogle-maps'])
    .config(googleMaps)
    .config(routing)
    .name;
