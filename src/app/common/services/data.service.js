import angular from 'angular';
import ngResource from 'angular-resource';
import ICAL from 'ical.js';

class DataService {

    static get $inject() {
      return ['$resource'];
    }

    constructor($resource) {
      this.$resource = $resource;
    }

    get festivals() {
      return this.$resource('http://citysdk.dmci.hva.nl/CitySDK/events/search', {category: 'festival'}, {
        query: {
          method: 'GET',
          isArray: true,
          transformResponse: function (data) {
            return angular.fromJson(data).event.map(value => {
              const jcalData = ICAL.parse(value.time[0].value);
              const vcalendar = new ICAL.Component(jcalData);
              const vevent = vcalendar.getFirstSubcomponent('vevent');
              const summary = vevent.getFirstPropertyValue('summary');
              const dateStart = vevent.getFirstPropertyValue('dtstart').toJSDate();

              return {
                id: value.id,
                targetPOI: value.location.relationship[0].targetPOI,
                summary,
                dateStart,
                //fake image
                image: 'http://www.iamsterdam.com/media/agenda/festivals/amsterdam-music-festival-nc.jpg?h=263'
              };
            });
          }
        }
      });
    }

    get eventInfo() {
      return this.$resource('http://citysdk.dmci.hva.nl/CitySDK/pois/:id', {id: '@id'});
    }

    getFestivals(params) {
      return this.festivals.query(params).$promise;
    }

    getEventInfo(id) {
      return this.eventInfo.get({id}).$promise;
    }
}

export default angular.module('services.data', [ngResource])
    .service('dataService', DataService)
    .name;


