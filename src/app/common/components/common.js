import angular from 'angular';
import Navbar from './navbar/navbar';
import Footer from './footer/footer';
import Spinner from './spinner/spinner';

export default angular.module('app.common.components', [
  Navbar,
  Footer,
  Spinner
]).name;
