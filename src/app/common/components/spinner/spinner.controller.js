class SpinnerController {

    get map() {
      return {
        small: {
          size: {radius: 15, width: 5, length: 20},
          class: 'spinner--small'
        },
        big: {
          size: {radius: 40, width: 8, length: 70},
          class: 'spinner--big'
        }
      };
    }

    constructor() {
      this.options = this.map[this.type || 'small'];
    }
}

export default SpinnerController;
