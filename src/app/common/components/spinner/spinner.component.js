import controller from './spinner.controller';
import template from './spinner.tpl.html';

let spinnerComponent = {
  restrict: 'E',
  scope: {},
  bindings: {type: '@'},
  template,
  controller,
  controllerAs: 'vm',
  bindToController: true
};

export default spinnerComponent;
