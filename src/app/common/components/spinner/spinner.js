import angular from 'angular';
import spinnerComponent from './spinner.component';

let spinnerModule = angular.module('spinner', [])
    .component('spinner', spinnerComponent)
    .name;

export default spinnerModule;
