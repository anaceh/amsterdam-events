import template from './footer.tpl.html';

let footerComponent = {
  restrict: 'E',
  template
};

export default footerComponent;
