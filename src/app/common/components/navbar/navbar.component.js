import template from './navbar.tpl.html';

let navbarComponent = {
  restrict: 'E',
  template
};

export default navbarComponent;
