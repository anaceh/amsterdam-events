### What is this repository for? ###

This project is about events in Amsterdam.  
It consists of two views: a list of events and info about the chosen event location.  
It uses AngularJS 1.5.8 with ES6 syntax.

Version - 0.0.1

### Set up instructions:

* Dev environment:  
	```npm install```  
	```npm run-script start```  
	```open http://localhost:3000/```  

* Prod environment:  
	```npm install```  
	```npm run-script build```  
	```open dist/index.html```  

### Coming soon:
* 1 Unit tests
* 2 e2e tests